include common-files/base.mk
SHELL := bash

# keep-sorted start
java8-badge: docker_badge_java8
java8-push: docker_push_java8
java8-test-rmi: docker_rmi_test_java8
java8-test: docker_build_test_java8
java8-topic: docker_topic_java8
java8: docker_build_java8
# keep-sorted end
